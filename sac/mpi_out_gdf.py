#!/usr/bin/env python
"""
Created on Mon Oct 21 14:05:06 2013

Parallel conversion from out to gdf
"""
import os

import numpy as np
from mpi4py import MPI
import h5py
import astropy.units as u

import pysac.io
import pysac.io.legacy.gdf_converter
import pysac.io.legacy.util
import pysac.io.gdf_writer

import sys
sys.path.append('../')
from scripts import sacconfig

comm = MPI.COMM_WORLD
rank = MPI.COMM_WORLD.rank

cfg = sacconfig.SACConfig()

output_path = cfg.gdf_dir
if rank == 0:
    if not os.path.exists(output_path):
        os.makedirs(output_path)

input_name = os.path.join(cfg.out_dir,
                        '3D_tube128_%s_%s_%03i.out'%(cfg.get_identifier(),
                                                    cfg.mpi_config, rank))

print '#', rank, '#:', input_name
vfile = pysac.io.legacy.VACfile(input_name)

mu = 1.25663706e-6
#==============================================================================
# Monkey patch varnames and make fields dictionary
#==============================================================================
def get_header_fields(vfile):
    header = vfile.header
    header['varnames'] = cfg.varnames
    indices = range(0,len(header['varnames']))
    w_ = dict(zip(header['varnames'],indices))
    
    #Convert w
    w = pysac.io.legacy.util.mag_convert(vfile.w,w_)
    fields = pysac.io.legacy.gdf_converter.convert_w_3D(np.ascontiguousarray(w), w_)
    
    for field in fields.values():
        unit = field['field'].unit
        x = np.ascontiguousarray(np.rollaxis(np.array(field['field']),0,3))
        field['field'] = u.Quantity(x, unit=unit)
    return header, fields

header, fields = get_header_fields(vfile)
#==============================================================================
# Decide how big the whole array is and what slice of it this processor has
#==============================================================================
nx = header['nx']
#Split sizes
n0 = 2
n1 = 2
n2 = 4

full_nx = [nx[0]*n0, nx[1]*n1, nx[2]*n2]
header['nx'] = full_nx
print rank, nx, full_nx, vfile.x.shape
coords = np.zeros(3)
if rank < n0:
    coords[0] = rank
elif (rank == n0):
    coords[1] = 1 
elif rank < (n0 + n1):
    coords[0] = rank - n0
    coords[1] = rank - n1
else:
    coords[2] = rank / (n0 * n1)
    rank2 = rank - (coords[2] * n2)
    
    if rank2 < n0:
        coords[0] = rank2
    elif rank2 == n0:
        coords[1] = 1
    elif rank2 < (n0 + n0):
        coords[0] = rank2 - n0
        coords[1] = rank2 - n1

s = map(int, coords * nx)
e = map(int, (coords+1) * nx)
arr_slice =  np.s_[s[1]:e[1],s[2]:e[2],s[0]:e[0]]

#==============================================================================
# Reconstruct the whole x array on all the processors
#==============================================================================
x_slice =  np.s_[:,s[0]:e[0],s[1]:e[1],s[2]:e[2]]
x = np.zeros(full_nx+[3])
print("*", rank, x.shape)
x[x_slice] = vfile.x

x_g = comm.gather(x, root=0)
if rank == 0:
    x_0 = np.sum(x_g,axis=0)
    x_xyz = np.zeros(x_0.shape)
    x_xyz[0] = np.rollaxis(x_0[1],0,3)#x
    x_xyz[1] = np.rollaxis(x_0[2],0,3)#y
    x_xyz[2] = np.rollaxis(x_0[0],0,3)#z
else:
    x_xyz = None

x = comm.bcast(x_xyz, root=0)
x *= u.meter

#==============================================================================
# Save a gdf file
#==============================================================================
for i in range(0,vfile.num_records,1):
    print '#', rank, '#', "read step %i"%i
    vfile.read_timestep(i+1)
    header, fields = get_header_fields(vfile)
    header['nx'] = full_nx

    f = h5py.File(os.path.join(output_path,
                               cfg.get_identifier()+'_%05i.gdf'%(i+1)),
                    'w', driver='mpio', comm=MPI.COMM_WORLD)

    pysac.io.gdf_writer.write_gdf(f, header, x, fields, arr_slice=arr_slice,
                                  data_author = "Stuart Mumford",
                                  data_comment = "Converted from outfiles in parallel")
print "rank %i finishes"%rank
