#!/bin/bash
#$ -l h_rt=99:00:00
#$ -cwd
#$ -l arch=intel*
#$ -N compress
#$ -P mhd
#$ -q mhd.q
#$ -j y

#Set the Python virtualenv
source ~/.bashrc
workon yt3

/home/smq11sjm/.virtualenvs/yt3/bin/python add_units_and_compress.py
_
echo "Job Complete"
/home/smq11sjm/.local/bin/pushover "Job Complete Compression 1"
