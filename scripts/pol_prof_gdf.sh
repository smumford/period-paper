#!/bin/bash
#$ -l h_rt=00:10:00
#$ -cwd -V
#$ -N Slog_prof
#$ -l np=16
#$ -j y

#Set the Python env
source ~/.bashrc

echo "SAC will run on the following nodes"
cat $PE_HOSTFILE

module load hdf5/1.8.9

echo "Run GDF Translator:"
cd $HOME/BitBucket/period-paper/sac
mpirun python -m cProfile -o gdf_128_low_coll.prof ./out2gdf_pure.py

echo "Job Complete"
