#!/home/smq11sjm/.virtualenvs/vtk6/bin/python
"""
Add units and compress.
"""

import os
import shutil
import subprocess

import glob2

import h5py

import yt
import numpy as np
import astropy.units as u

all_files = glob2.glob('/fastdata/smq11sjm/PeriodPaperB15/gdf/**/*B015_0*gdf')

out_path = '/fastdata/smq11sjm/PeriodPaperB15/gdf_gzip/'

for afile in all_files:
    in_file_name = afile
    out_file_name = os.path.join(out_path, afile.split('/gdf/')[1])
    tmp_file_name = out_file_name+'_tmp'
    
    directory = os.path.dirname(out_file_name)
    if not os.path.exists(directory):
        os.makedirs(directory)

    shutil.copyfile(in_file_name, tmp_file_name)

    in_file = h5py.File(tmp_file_name, 'r+')
    
    # "dataset_units" group
    g = in_file.create_group("dataset_units")
    BASE = [("length_unit", "m"), ("mass_unit", "kg"), ("time_unit", "s"),
            ("velocity_unit", "m / s"), ("magnetic_unit", "T")]
    for base_name, base_unit in BASE:
        in_file["dataset_units"][base_name] = 1.0
        in_file["dataset_units"][base_name].attrs["unit"] = base_unit
    
    units = {'velocity_x': 1*u.Unit('m/s'),
             'velocity_y': 1*u.Unit('m/s'),
             'velocity_z': 1*u.Unit('m/s'),
             'density_bg': 1*u.Unit('kg/m^3'),
             'density_pert': 1*u.Unit('kg/m^3'),
             'internal_energy_bg': 1*u.Unit('Pa'),
             'internal_energy_pert': 1*u.Unit('Pa'),
             'mag_field_x_bg': 1*u.Unit('T'),
             'mag_field_y_bg': 1*u.Unit('T'),
             'mag_field_z_bg': 1*u.Unit('T'),
             'mag_field_x_pert': 1*u.Unit('T'),
             'mag_field_y_pert': 1*u.Unit('T'),
             'mag_field_z_pert': 1*u.Unit('T')}
    
    for field in in_file['field_types'].keys():
        ytunit = np.string_(yt.YTQuantity.from_astropy(units[field]).units)
        in_file["dataset_units"][field] = 1.0
        in_file["dataset_units"][field].attrs["unit"] = ytunit
    
    
    in_file.close()
    
    subprocess.call(['h5repack', '-v', '-f', 'GZIP=9', tmp_file_name, out_file_name])
    
    os.remove(tmp_file_name)
